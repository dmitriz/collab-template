# Collaboration project template

Central point for collabortion on X
(information, discussion, planning, links etc).

This project is confidential. 
Membership is by invitation and access is restricted to members.
New invitations need approval by all members.

#### [Information about access to this project](Access.md)

## Discussion (issues) and content (directory)

### Reading issues

To see the list of all `issues`,
click on `Issues` tab in the left navigation panel (3rd icon from the top). 
You can `search`, `filter` or `sort` the `issues` by `labels`.

When you see any `issue` of interest, you can enter and add your `comments` at the bottom, 
or below each `comment` or `comment thread`.


### Email notifications
You can subscribe to `notifications` for any `issue` by `toggling` the `notification` switch
at the very bottom in the right navigation panel (you may need to `scroll down`).


### Open new issue

To start new `issue` go to the list of all `issues` as described above, 
and click the green button `New issue` in the right top corner.

You can simply add `title` and `description` (skipping all other fields), 
and click on the green `Submit issue` button.

### Files and directories
To see all `files` and `directories` stored on this site, 
click on `Repository` tab in the left navigation panel (2nd icon from the top). 

### Formatting
For advanced formatting, `headers`, `lists`, `itemization`, `images`, `videos`, `mathematics formulas` 
and much more, use the Markdown language https://docs.gitlab.com/ee/user/markdown.html
